﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MurkaTask.Components {

    public class TailProcessor {

        public float strokeWidth;
        public Mesh mesh;
        protected Vector2 lastPoint;
        public int length;
        public Vector2 normalizedDistance;
        public List<Vector3> points;


        public TailProcessor() {
            init();
        }

        protected void init() {
            strokeWidth = 9f / Screen.width;
            points = new List<Vector3>();
            reset();
        }


        /// <summary>
        /// calculates two new points that corners of the line and adds them to points list (for render)
        /// </summary>
        /// <param name="point"></param>
        public void addPoint(Vector2 point) {
            float magnitude;

            point.x /= Screen.width;
            point.y /= Screen.height;

            if (length > 0) {
                normalizedDistance.x = point.x - lastPoint.x;
                normalizedDistance.y = point.y - lastPoint.y;
                magnitude = normalizedDistance.magnitude;

                if (magnitude == 0) {
                    return;
                }

                normalizedDistance /= magnitude;
                //normal left
                points.Add(new Vector3(point.x + normalizedDistance.y * strokeWidth, point.y - normalizedDistance.x * strokeWidth, 0));
                //normal right
                points.Add(new Vector3(point.x - normalizedDistance.y * strokeWidth, point.y + normalizedDistance.x * strokeWidth, 0));

            }
            else {
                points.Add(new Vector3(point.x - strokeWidth, point.y, 0));
                points.Add(new Vector3(point.x + strokeWidth, point.y, 0));
            }

            lastPoint = point;
            length++;
        }

        /// <summary>
        /// clears points array and sets flags
        /// </summary>
        public void reset() {
            length = 0;
            points.Clear();
        }


        /// <summary>
        /// renders tail
        /// </summary>
        /// <param name="mat"></param>
        public void drawGL(Material mat) {
            GL.PushMatrix();
            mat.SetPass(0);
            GL.LoadOrtho();
            GL.Begin(GL.QUADS);
            GL.Color(new Color(1, 1, 1, 1));

            float pointsCount = points.Count - 2;
            float alphaCount = pointsCount * 2;

            for (int i = 0; i < pointsCount - 2; i += 2) {
                GL.Color(new Color(0.8f, 0.8f, 1, (float)i / alphaCount));
                GL.Vertex(points[i + 1]);
                GL.Vertex(points[i]);
                GL.Vertex(points[i + 2]);
                GL.Vertex(points[i + 3]);
            }
            GL.End();

            GL.PopMatrix();
        }



    }
}