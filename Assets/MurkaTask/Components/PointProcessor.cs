﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MurkaTask.Components {
    public class PointProcessor {

        public int maxPoints = 60;
        public Queue<Vector2> points;

        public PointProcessor() {
            init();
        }

        protected void init() {
            points = new Queue<Vector2>();
        }

        /// <summary>
        /// adds points from input device
        /// </summary>
        /// <param name="data"></param>
        public void addPoints(Vector2 data) {
            points.Enqueue(data);
        }

        public Vector2[] getPointsArray() {
            return points.ToArray();
        }

        public void clearPoints() {
            points.Clear();
        }

        /// <summary>
        /// updates logics, recommend to call from MonoBehaviour::FixedUpdate
        /// </summary>
        public void update() {
            /*if (points.Count > 0) {
                points.Dequeue();
            }*/


        }
    }
}