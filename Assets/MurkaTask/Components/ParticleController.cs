﻿using UnityEngine;
using System.Collections;

namespace MurkaTask.Components {

    public class ParticleController {

        public ParticleSystem particle;
        public TailProcessor tailProcessor;

        public ParticleController(ParticleSystem ps, TailProcessor tp) {
            particle = ps;
            tailProcessor = tp;
        }

        public void move() {
            particle.transform.localPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            particle.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(tailProcessor.normalizedDistance.x, tailProcessor.normalizedDistance.y));
            particle.Emit(5);
        }


        public void pause() {
            particle.Emit(0);
        }




    }
}