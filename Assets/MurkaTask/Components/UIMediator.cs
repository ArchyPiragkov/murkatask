﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace MurkaTask.Components {

    public enum UIStates {
        waitForNewGame, game
    }


    public class UIMediator : MonoBehaviour {

        public Text     scoreText;
        public Text     timeText;
        public Button   resetButton;
        public Text     rezultText;
        
        void Start() {
            newGameScreen();
            scoreText.text = "";
        }


        public void newGameScreen(int score = 0) {
            rezultText.text = "Score: " + score;
            scoreText.gameObject.SetActive(false);
            timeText .gameObject.SetActive(false);
            resetButton.gameObject.SetActive(true);
            rezultText. gameObject.SetActive(true);
        }

        public void setTime(float seconds) {
            timeText.text = ((int)seconds ).ToString() + ":" + ((int)((seconds - (int)seconds)*100));
        }

        public void setScore(int score) {
            scoreText.text = score.ToString();
        }

        public void gameScreen() {
            setScore(0);
            scoreText.gameObject.SetActive(true);
            timeText.gameObject.SetActive(true);
            resetButton.gameObject.SetActive(false);
            rezultText.gameObject.SetActive(false);         
        }

        public void setNewGameListener(Action callb) {
            resetButton.onClick.AddListener(new UnityEngine.Events.UnityAction(callb));
        }



    }
}