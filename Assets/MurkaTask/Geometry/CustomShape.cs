﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace MurkaTask.Geometry {
    public class CustomShape:Shapes {

        public override void initFigures() {
            shapesList = new List<shapeStruct>();

            shapesList.Add(new shapeStruct( new[] { new Vector2(), new Vector2(2, 3), new Vector2(4, 0), new Vector2() }, "triangle", 0));
            shapesList.Add(new shapeStruct( new[] { new Vector2(), new Vector2(0, 3), new Vector2(2, 3)}, "g", 0));
            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(0.5f,0), new Vector2(2, 1), new Vector2(3.5f, 0),
                                                        new Vector2(3,2), new Vector2(4, 3), new Vector2(3, 3),
                                                        new Vector2(2,5), new Vector2(1, 3), new Vector2(0, 3),
                                                        new Vector2(1,2), new Vector2(0.5f,0)
                                                    }, "star", 5));

            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(), new Vector2(1, 1), new Vector2(2, 0),
                                                        new Vector2(3,1), new Vector2(4, 0), new Vector2(6, 2),
                                                        new Vector2(4,4), new Vector2(3, 3), new Vector2(2, 4),
                                                        new Vector2(1,3), new Vector2(0,4)
                                                    }, "star", 5));
            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(), new Vector2(0, 2), new Vector2(1, 2),
                                                        new Vector2(2,2), new Vector2(2, 0), new Vector2(1, 1),
                                                        new Vector2(0,0)
                                                    }, "up", 5));

            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(0,2), new Vector2(2, 2), new Vector2(3, 1),
                                                        new Vector2(3,0), new Vector2(5, 0), new Vector2(5, 2),
                                                        new Vector2(4,2), new Vector2(4, 3), new Vector2(3, 3),
                                                        new Vector2(3,4), new Vector2(6,4), new Vector2(6,3)
                                                    }, "strange", 5));

            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(0,0), new Vector2(0, 4), new Vector2(0, 2),
                                                        new Vector2(2,4), new Vector2(1.5f, 4.5f)
                                                    }, "rune", 5));
            shapesList.Add(new shapeStruct(new[]    {
                                                        new Vector2(4,0), new Vector2(1, 0), new Vector2(0, 1),
                                                        new Vector2(0,4), new Vector2(1, 5), new Vector2(4, 5),
                                                        new Vector2(5,4), new Vector2(5, 2), new Vector2(4, 1),
                                                        new Vector2(2,1), new Vector2(1,2), new Vector2(1, 3),
                                                        new Vector2(2,4), new Vector2(3, 4), new Vector2(4, 3),
                                                        new Vector2(3,2), new Vector2(2,2), new Vector2(2, 3)
                                                    }, "spiral", 5));


            interpolateAllShapes();

        }

    }
}
