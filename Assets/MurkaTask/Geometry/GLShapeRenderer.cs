﻿using System;
using System.Collections.Generic;
using UnityEngine;



namespace MurkaTask.Geometry {
    public class GLShapeRenderer {
        public Material shapeMaterial;
        protected Vector2[] shape;


        public void glAdaptShape(Vector2[] _shape) {
            if (_shape == null) {
                shape = null;
                return;
            }
            Rect r1, r2;
            shape = Shapes.tryEqualBounds(new Vector2[] { new Vector2(), new Vector2(0.4f, 0.4f) }, (Vector2[])_shape.Clone(), out r1, out r2);
            for (int i = 0; i < shape.Length; i++) {
                shape[i].x += 0.3f;
                shape[i].y += 0.3f;
            }
        }

        public void renderShape() {
            if (shape == null) {
                return;
            }
            GL.PushMatrix();
            shapeMaterial.SetPass(0);
            GL.LoadOrtho();
            GL.Begin(GL.LINES);
            GL.Color(new Color(1, 1, 1, 1));
            float pointsCount = shape.Length;
            //float alphaCount = pointsCount * 2;

            for (int i = 0; i < pointsCount; i++) {
                GL.Vertex(shape[i]);
            }
            GL.End();
            GL.PopMatrix();
        }


    }
}
