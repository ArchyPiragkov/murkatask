﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MurkaTask.Geometry {

    public class shapeStruct {
        public shapeStruct(Vector2[] shape, string name, int hardness) {
            this.shape = shape;
            this.hardness = hardness;
            this.name = name;
        }
        public Vector2[]    shape;
        public string       name;
        public int          hardness;
    }

    public class Shapes {

        public List<shapeStruct> shapesList;
        protected int lastShape;
        /*
            need to be overriden with list of shapes, like in CustomShape class
         */
        public virtual void initFigures() {

        }

        /*public Vector2[] getShape(string name) {
            
            //return interpolateShape(shapesList[name]);
        }*/

        public Vector2[] getShape() {
            while (true && shapesList.Count>1) {
                int id = Random.Range(0, shapesList.Count);
                if (id != lastShape) {
                    lastShape = id;
                    return shapesList[id].shape;
                }
            }
            return shapesList[0].shape;
        }


        public static Vector2[] interpolateShape(Vector2[] array, int steps = 20) {
            if (array.Length < 2) {
                return array;
            }
            int calcLen = (array.Length - 1) * steps + 1;
            int pos = 0;
            Vector2[] tmpArr = new Vector2[calcLen];
            Vector2 distance;

            for (int i = 1; i < array.Length; i++) {
                pos = (i - 1) * steps;
                distance = array[i] - array[i - 1];
                for (int j = 0; j <= steps; j++) {
                    tmpArr[pos + j] = array[i - 1] + (distance / (steps)) * j;
                }
            }

            return tmpArr;
        }

        public static Vector2[] tryEqualBounds(Vector2[] original, Vector2[] created, out Rect origBounds, out Rect compareBounds) {
            origBounds     = calcBounds(original);
            Rect createdBounds  = calcBounds(created);
            Rect offset = new Rect( 
                                    origBounds.x - createdBounds.x,
                                    origBounds.y - createdBounds.y, 
                                    origBounds.width / createdBounds.width,
                                    origBounds.height/ createdBounds.height
                                    //origBounds.width / createdBounds.width
                                    );

            //Matrix4x4 mat = new Matrix4x4();

            for (int i = 0; i < created.Length; i++) {
                created[i].x *= offset.width;
                created[i].y *= offset.height;
                created[i].x += offset.x * offset.width;
                created[i].y += offset.y * offset.height;
            }

            compareBounds = calcBounds(created);       

            return created;
        }

        public static Rect calcBounds(Vector2[] array) {
            Rect bounds = new Rect(array[0].x, array[0].y, array[0].x, array[0].y);

            for (int i = 0; i < array.Length; i++) {
                if (array[i].x < bounds.x) bounds.x = array[i].x;
                if (array[i].x > bounds.width) bounds.width = array[i].x;
                if (array[i].y < bounds.y) bounds.y = array[i].y;
                if (array[i].y > bounds.height) bounds.height = array[i].y;
            }
            bounds.width  -= bounds.x;
            bounds.height -= bounds.y;

            return bounds;
        }

        /// <summary>
        /// Compares two cutless shapes that created by set of Vector2 points. On first step it compares shape's AABB bounds proportions, makes point by point comparation
        /// </summary>
        /// <param name="original">original shape to compare</param>
        /// <param name="comparer">secondary shape that nedd to be compared with original</param>
        /// <param name="AABBKof">Minimal rejection of AABB proportions, from 0 to endless</param>
        /// <param name="distanceKof">Allowed distance between points to pass test. value is persentage of AABB diagonal (from 0 to 1)</param>
        /// <param name="percentToSucces">Persentage of correct distance tests to get succes comparation. From 0 to 1</param>
        /// <returns></returns>
        public static bool compareShapes(Vector2[] original, Vector2[] comparer, /*float AABBKof = 0.2f, */float distanceKof = 0.04f, float percentToSucces = 0.95f) {
            Rect origBounds;
            Rect comparerBounds;

            Vector2[] fixedBounds = tryEqualBounds(original, comparer, out origBounds, out comparerBounds);

            //--- First step: compares rejection of AABB
            float maxRejection;
            float rejectionX = 1;
            float rejectionY = 1;

            /*if (origBounds.width > comparerBounds.width) {
                rejectionX = origBounds.width / comparerBounds.width - 1;
            }
            else {
                rejectionX = comparerBounds.width / origBounds.width - 1;
            }

            if (origBounds.height > comparerBounds.height) {
                rejectionY = origBounds.height / comparerBounds.height - 1;
            }
            else {
                rejectionY = comparerBounds.height / origBounds.height - 1;
            }

            maxRejection = Mathf.Max(rejectionX, rejectionY);
            if (maxRejection >= AABBKof) {
                return false;
            }*/

            //--- Second step: compares points distances

            float calcDist = Mathf.Sqrt(Mathf.Pow(origBounds.width, 2) + Mathf.Pow(origBounds.height, 2)) * distanceKof;
            maxRejection = 1 - percentToSucces;
            //re-using rejectionX var to count max allowed fails 
            rejectionX = (float)1 / original.Length;
            //re-using rejectionY var to count current fails
            rejectionY = 0;

            float currentDist;
            int j = 0;
            for (int i = 0; i < original.Length; i++) {
                for (j = 0; j < comparer.Length; j++) {
                    currentDist = Mathf.Sqrt(Mathf.Pow(original[i].x - comparer[j].x, 2) + Mathf.Pow(original[i].y - comparer[j].y, 2));
                    if (currentDist < calcDist) {
                        break;
                    }
                }
                if (j == comparer.Length) {
                    rejectionY += rejectionX;
                    if (rejectionY > maxRejection) {
                        Debug.Log("iteration: " + i + "/" +original.Length);
                        return false;
                    }
                }   
            }
            
            return true;
        }

        protected void interpolateAllShapes() {
            foreach (var sh in shapesList) {
                sh.shape = interpolateShape(sh.shape);
            }
        }



    }
}