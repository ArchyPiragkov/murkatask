﻿using UnityEngine;
using System.Collections;

public static class Utils  {

    public static Vector2 normaLeft(this Vector2 vec) {
        return new Vector2(vec.y, -vec.x);
    }

    public static Vector2 normaRight(this Vector2 vec) {
        return new Vector2(-vec.y, vec.x);
    }
    
}
