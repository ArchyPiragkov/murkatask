﻿using UnityEngine;
using System.Collections;
using MurkaTask.Components;
using MurkaTask.Geometry;

namespace MurkaTask {

    public class GameModel : MonoBehaviour {

        [Range(0,1)]
        public float                    distanceKof = 0.04f;
        [Range(0,1)]
        public float                    percentToSucces = 0.95f;

        public ParticleSystem           tailParticle;
        public Material                 tailMaterial;
        public UIMediator               uiMediator;

        protected PointProcessor        pointProcessor;
        protected TailProcessor         tailProcessor;
        protected ParticleController    particleController;
        protected CustomShape           shapes;
        protected GLShapeRenderer       glShapeRender;
        protected bool                  mouseDown;

        protected Vector2[]             currentShape;
        protected float                 timeLeft;
        protected float                   timeDelta;
        protected int                   score;
        protected Coroutine             timerCoroutine;
        //protected Vector2[]             glAdaptedShape;

        void Awake() {
            pointProcessor = new PointProcessor();
            tailProcessor = new TailProcessor();

            particleController = new ParticleController(tailParticle, tailProcessor);
            particleController.pause();

            shapes = new CustomShape();
            shapes.initFigures();

            glShapeRender = new GLShapeRenderer();
            glShapeRender.shapeMaterial = tailMaterial;
            glShapeRender.glAdaptShape(null);
            //renewShape();

            uiMediator.setNewGameListener(newGame);
            

        }

        void Start() {

        }

        void FixedUpdate() {
            if (Input.GetMouseButton(0)) {
                mouseDown = true;
                pointProcessor.addPoints(Input.mousePosition);
                tailProcessor.addPoint(Input.mousePosition);
                particleController.move();

            }
            else {
                if (mouseDown) {

                    if (timeLeft > 0) {
                        bool rezult = CustomShape.compareShapes(currentShape, pointProcessor.getPointsArray(), distanceKof, percentToSucces);
                        if (rezult) {
                            renewShape();
                        }
                    }
                    particleController.pause();
                    pointProcessor.clearPoints();
                    tailProcessor.reset();
                    mouseDown = false;
                }
            }

            updateTimer();
        }

        public void OnPostRender() {
            if (tailProcessor.length > 0) {
                tailProcessor.drawGL(tailMaterial);
            }
            glShapeRender.renderShape();
        }

        /// <summary>
        /// laods next random shape from custom shapes container, sets it as current shape and prepare for GL render
        /// </summary>
        protected void getNextShape() {
            currentShape = shapes.getShape();
            glShapeRender.glAdaptShape(currentShape);
        }

        /// <summary>
        /// calls when user succesfuly drawn shape. updates score, shape and timer
        /// </summary>
        protected void renewShape() {
            getNextShape();
            //timeDelta = (int)Mathf.Round(timeDelta * 0.95f);
            timeDelta *= 0.95f;
            timeLeft = timeDelta;
            score++;
            uiMediator.setScore(score);
        }

        /// <summary>
        /// stats new game
        /// </summary>
        protected void newGame() {
            score = 0;
            timeDelta = 50;
            timeLeft = timeDelta;
            uiMediator.gameScreen();
            getNextShape();
        }

        /// <summary>
        /// finishes game
        /// </summary>
        protected void gameOver() {
            timeLeft = 0;
            uiMediator.newGameScreen(score);
            glShapeRender.glAdaptShape(null);
        }

        /// <summary>
        /// update timer value, screens it and controls for time left
        /// </summary>
        protected void updateTimer() {
            if (timeLeft > 0) {
                timeLeft -= Time.deltaTime;
                if (timeLeft <= 0) {
                    gameOver();
                    return;
                }
                uiMediator.setTime(timeLeft);
            }
        }
        


    }
}